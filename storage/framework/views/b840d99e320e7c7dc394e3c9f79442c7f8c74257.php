<?php $__env->startSection('content'); ?>
    <?php
        $languages = \Modules\Language\Models\Language::getActive();
    ?>
    <form method="post" action="<?php echo e(route('seller.gig.store', ['id'=>($row->id) ? $row->id : '-1','lang'=>request()->query('lang')] )); ?>" class="default-form" >
        <?php echo csrf_field(); ?>
        <input type="hidden" name="id" value="<?php echo e($row->id); ?>">
        <div class="upper-title-box">
            <div class="row">
                <div class="col-md-9">
                    <h3><?php echo e($row->id ? __('Edit: ').$row->title : __('Add new gig')); ?></h3>
                    <div class="text">
                        <?php if($row->slug): ?>
                            <p class="item-url-demo"><?php echo e(__("Permalink")); ?>: <?php echo e(url('gig' )); ?>/<a href="#" class="open-edit-input" data-name="slug"><?php echo e($row->slug); ?></a>
                            </p>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <?php if($row->slug): ?>
                        <a class="theme-btn btn-style-one" href="<?php echo e($row->getDetailUrl(request()->query('lang'))); ?>" target="_blank"><?php echo e(__("View Gig")); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php echo $__env->make('admin.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php if($row->id): ?>
            <?php echo $__env->make('Language::admin.navigation', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endif; ?>

        <div class="row">
            <div class="col-xl-9">
                <!-- Ls widget -->
                <div class="ls-widget">
                    <div class="tabs-box">
                        <div class="widget-title"><h4><?php echo e(__("Overview")); ?></h4></div>
                        <div class="widget-content">
                            <div class="form-group">
                                <label><?php echo e(__("Title")); ?> <span class="text-danger">*</span></label>
                                <input type="text" name="title" value="<?php echo e(old('title',$translation->title)); ?>" required placeholder="<?php echo e(__("Name of the gig")); ?>" class="form-control">
                            </div>

                            <?php if(is_default_lang()): ?>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label><?php echo e(__("Category Level 1")); ?> <span class="text-danger">*</span></label>
                                        <select name="cat_id" required class="form-control">
                                            <option value=""><?php echo e(__("-- Select a Category--")); ?></option>
                                            <?php
                                            $items = \Modules\Gig\Models\GigCategory::query()->whereNull('parent_id')->get();
                                            ?>
                                            <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option <?php if(old('cat_id',$row->cat_id) == $item->id): ?> selected <?php endif; ?> value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label><?php echo e(__("Category Level 2")); ?> <span class="text-danger">*</span></label>
                                        <select name="cat2_id" required class="form-control">
                                            <option value=""><?php echo e(__("-- Select a Subcategory --")); ?></option>
                                            <?php
                                            $items = \Modules\Gig\Models\GigCategory::query()->withDepth()->having('depth', '=', 1)->get();
                                            ?>
                                            <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option data-parent="<?php echo e($item->parent_id); ?>" <?php if(old('cat2_id',$row->cat2_id) == $item->id): ?> selected <?php endif; ?> value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label><?php echo e(__("Category Level 3")); ?> <span class="text-danger">*</span></label>
                                        <select name="cat3_id" required class="form-control">
                                            <option value=""><?php echo e(__("-- Select a Subject--")); ?></option>
                                            <?php
                                            $items = \Modules\Gig\Models\GigCategory::query()->withDepth()->having('depth', '=', 2)->get();
                                            ?>
                                            <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option data-parent="<?php echo e($item->parent_id); ?>" <?php if(old('cat3_id',$row->cat3_id) == $item->id): ?> selected <?php endif; ?> value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><?php echo e(__("Search Tags")); ?></label>
                                    <div class="">
                                        <input type="text" data-role="tagsinput" value="<?php echo e($row->tag); ?>" placeholder="<?php echo e(__('Enter tag')); ?>" name="tag" class="form-control tag-input">
                                        <div class="show_tags">
                                            <?php if(!empty($tags)): ?>
                                                <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <span class="tag_item"><?php echo e($tag->name); ?><span data-role="remove"></span>
                                                        <input type="hidden" name="tag_ids[]" value="<?php echo e($tag->id); ?>">
                                                    </span>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <p class="text-right mb-0"><small><?php echo e(__("10 tags maximum")); ?></small></p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <?php
                    $packages = old('packages',$translation->packages);
                ?>
                <div class="ls-widget">
                    <div class="tabs-box">
                        <div class="widget-title"><h4><?php echo e(__("Scope & Pricing")); ?></h4></div>
                        <div class="widget-content">
                            <div class="form-group">
                                <label><?php echo e(__("Packages")); ?></label>
                                <div class="form-group-item">
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-3">&nbsp;</div>
                                            <div class="col-md-3"><?php echo e(__("Basic")); ?></div>
                                            <div class="col-md-3"><?php echo e(__("Standard")); ?></div>
                                            <div class="col-md-3"><?php echo e(__("Premium")); ?></div>
                                        </div>
                                    </div>
                                    <div class="g-items">
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-md-3 text-center">
                                                    <strong><?php echo e(__(" Name")); ?> <span class="text-danger">*</span></strong>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" required name="packages[0][name]" class="form-control" value="<?php echo e($packages[0]['name'] ?? 'Basic'); ?>" placeholder="<?php echo e(__('Name your package')); ?>">
                                                    <input type="hidden" name="packages[0][key]" value="basic">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="packages[1][name]" class="form-control" value="<?php echo e($packages[1]['name'] ?? 'Standard'); ?>" placeholder="<?php echo e(__('Name your package')); ?>">
                                                    <input type="hidden" name="packages[1][key]" value="standard">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="packages[2][name]" class="form-control" value="<?php echo e($packages[2]['name'] ?? 'Premium'); ?>" placeholder="<?php echo e(__('Name your package')); ?>">
                                                    <input type="hidden" name="packages[2][key]" value="premium">
                                                </div>
                                            </div>
                                        </div>
                                        <?php if(is_default_lang()): ?>
                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-md-3 text-center">
                                                        <strong><?php echo e(__("Price")); ?> <span class="text-danger">*</span></strong>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" step="any" name="basic_price" min="5" class="form-control" required value="<?php echo e($row->basic_price); ?>" placeholder="<?php echo e(__('Package Price')); ?>">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" step="any" name="standard_price" class="form-control" value="<?php echo e($row->standard_price); ?>" placeholder="<?php echo e(__('Package Price')); ?>">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" step="any" name="premium_price" class="form-control" value="<?php echo e($row->premium_price); ?>" placeholder="<?php echo e(__('Package Price')); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-md-3 text-center">
                                                    <strong><?php echo e(__("Desc")); ?> <span class="text-danger">*</span></strong>
                                                </div>
                                                <div class="col-md-3">
                                                    <textarea name="packages[0][desc]" class="form-control" required placeholder="<?php echo e(__('Describe the details of your offering')); ?>" cols="30" rows="6"><?php echo e($packages[0]['desc'] ?? ''); ?></textarea>
                                                </div>
                                                <div class="col-md-3">
                                                    <textarea name="packages[1][desc]" class="form-control" placeholder="<?php echo e(__('Describe the details of your offering')); ?>" cols="30" rows="6"><?php echo e($packages[1]['desc'] ?? ''); ?></textarea>
                                                </div>
                                                <div class="col-md-3">
                                                    <textarea name="packages[2][desc]" class="form-control" placeholder="<?php echo e(__('Describe the details of your offering')); ?>" cols="30" rows="6"><?php echo e($packages[2]['desc'] ?? ''); ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if(is_default_lang()): ?>
                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-md-3 text-center">
                                                        <strong><?php echo e(__("Delivery Time")); ?> <span class="text-danger">*</span></strong>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select name="packages[0][delivery_time]" required class="form-control">
                                                            <option value=""><?php echo e(__("-- Please Select --")); ?></option>
                                                            <?php for($i = 1; $i <= 10; $i++): ?>
                                                                <option <?php if(($packages[0]['delivery_time'] ?? '') == $i): ?> selected <?php endif; ?> value="<?php echo e($i); ?>"><?php echo e(__(":count Day(s)",['count'=>$i])); ?></option>
                                                            <?php endfor; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select name="packages[1][delivery_time]" class="form-control">
                                                            <option value=""><?php echo e(__("-- Please Select --")); ?></option>
                                                            <?php for($i = 1; $i <= 10; $i++): ?>
                                                                <option <?php if(($packages[1]['delivery_time'] ?? '') == $i): ?> selected <?php endif; ?> value="<?php echo e($i); ?>"><?php echo e(__(":count Day(s)",['count'=>$i])); ?></option>
                                                            <?php endfor; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select name="packages[2][delivery_time]" class="form-control">
                                                            <option value=""><?php echo e(__("-- Please Select --")); ?></option>
                                                            <?php for($i = 1; $i <= 10; $i++): ?>
                                                                <option <?php if(($packages[2]['delivery_time'] ?? '') == $i): ?> selected <?php endif; ?> value="<?php echo e($i); ?>"><?php echo e(__(":count Day(s)",['count'=>$i])); ?></option>
                                                            <?php endfor; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-md-3 text-center">
                                                        <strong><?php echo e(__("Revisions")); ?> <span class="text-danger">*</span></strong>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select name="packages[0][revision]" required class="form-control">
                                                            <option value=""><?php echo e(__("-- Please Select --")); ?></option>
                                                            <?php for($i = 1; $i <= 10; $i++): ?>
                                                                <option <?php if(($packages[0]['revision'] ?? '') == $i): ?> selected <?php endif; ?> value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                                            <?php endfor; ?>
                                                            <option value="-1"><?php echo e(__("Unlimited")); ?></option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select name="packages[1][revision]" class="form-control">
                                                            <option value=""><?php echo e(__("-- Please Select --")); ?></option>
                                                            <?php for($i = 1; $i <= 10; $i++): ?>
                                                                <option <?php if(($packages[1]['revision'] ?? '') == $i): ?> selected <?php endif; ?> value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                                            <?php endfor; ?>
                                                            <option value="-1"><?php echo e(__("Unlimited")); ?></option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select name="packages[2][revision]" class="form-control">
                                                            <option value=""><?php echo e(__("-- Please Select --")); ?></option>
                                                            <?php for($i = 1; $i <= 10; $i++): ?>
                                                                <option <?php if(($packages[2]['revision'] ?? '') == $i): ?> selected <?php endif; ?> value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                                            <?php endfor; ?>
                                                            <option value="-1"><?php echo e(__("Unlimited")); ?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><?php echo e(__("Package Compare")); ?></label>
                                <div class="form-group-item">
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-5"><?php echo e(__("Name")); ?></div>
                                            <div class="col-md-2"><?php echo e(__('Basic')); ?></div>
                                            <div class="col-md-2"><?php echo e(__('Standard')); ?></div>
                                            <div class="col-md-2"><?php echo e(__('Premium')); ?></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items">
                                        <?php $old = old('package_compare',$translation->package_compare ?? []);
                                        if(empty($old)) $old = [[]];
                                        ?>
                                        <?php if(!empty($old)): ?>
                                            <?php $__currentLoopData = $old; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$extra_price): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="item" data-number="<?php echo e($key); ?>">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <?php if(!empty($languages) && setting_item('site_enable_multi_lang') && setting_item('site_locale')): ?>
                                                                <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php $key_lang = setting_item('site_locale') != $language->locale ? "_".$language->locale : ""   ?>
                                                                    <div class="g-lang">
                                                                        <div class="title-lang"><?php echo e($language->name); ?></div>
                                                                        <input type="text" name="package_compare[<?php echo e($key); ?>][name<?php echo e($key_lang); ?>]" class="form-control" value="<?php echo e($extra_price['name'.$key_lang] ?? ''); ?>" placeholder="<?php echo e(__('Attribute Name')); ?>">
                                                                    </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php else: ?>
                                                                <input type="text" name="package_compare[<?php echo e($key); ?>][name]" class="form-control" value="<?php echo e($extra_price['name'] ?? ''); ?>" placeholder="<?php echo e(__('Attribute Name')); ?>">
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="text"  name="package_compare[<?php echo e($key); ?>][content]" class="form-control" value="<?php echo e($extra_price['content'] ?? ''); ?>">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="text"  name="package_compare[<?php echo e($key); ?>][content1]" class="form-control" value="<?php echo e($extra_price['content1'] ?? ''); ?>">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="text"  name="package_compare[<?php echo e($key); ?>][content2]" class="form-control" value="<?php echo e($extra_price['content2'] ?? ''); ?>">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <?php if(is_default_lang()): ?>
                                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="la la-trash"></i></span>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="text-right">
                                        <?php if(is_default_lang()): ?>
                                            <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> <?php echo e(__('Add item')); ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="g-more hide">
                                        <div class="item" data-number="__number__">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <?php if(!empty($languages) && setting_item('site_enable_multi_lang') && setting_item('site_locale')): ?>
                                                        <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php $key = setting_item('site_locale') != $language->locale ? "_".$language->locale : ""   ?>
                                                            <div class="g-lang">
                                                                <div class="title-lang"><?php echo e($language->name); ?></div>
                                                                <input type="text" __name__="package_compare[__number__][name<?php echo e($key); ?>]" class="form-control" value="" placeholder="<?php echo e(__('Attribute name')); ?>">
                                                            </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php else: ?>
                                                        <input type="text" __name__="package_compare[__number__][name]" class="form-control" value="" placeholder="<?php echo e(__('Attribute Name')); ?>">
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" __name__="package_compare[__number__][content]" class="form-control" value="">
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" __name__="package_compare[__number__][content1]" class="form-control" value="">
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" __name__="package_compare[__number__][content2]" class="form-control" value="">
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="la la-trash"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><?php echo e(__("Add Extra Services")); ?></label>
                                <div class="form-group-item">
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-5"><?php echo e(__("Name")); ?></div>
                                            <div class="col-md-3"><?php echo e(__('Price')); ?></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items">
                                        <?php $old = old('extra_price',$row->extra_price ?? []);
                                        if(empty($old)) $old = [[]];
                                        ?>
                                        <?php if(!empty($old)): ?>
                                            <?php $__currentLoopData = $old; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$extra_price): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="item" data-number="<?php echo e($key); ?>">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <?php if(!empty($languages) && setting_item('site_enable_multi_lang') && setting_item('site_locale')): ?>
                                                                <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php $key_lang = setting_item('site_locale') != $language->locale ? "_".$language->locale : ""   ?>
                                                                    <div class="g-lang">
                                                                        <div class="title-lang"><?php echo e($language->name); ?></div>
                                                                        <input type="text" name="extra_price[<?php echo e($key); ?>][name<?php echo e($key_lang); ?>]" class="form-control" value="<?php echo e($extra_price['name'.$key_lang] ?? ''); ?>" placeholder="<?php echo e(__('Extra price name')); ?>">
                                                                    </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php else: ?>
                                                                <input type="text" name="extra_price[<?php echo e($key); ?>][name]" class="form-control" value="<?php echo e($extra_price['name'] ?? ''); ?>" placeholder="<?php echo e(__('Extra price name')); ?>">
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <input type="number" <?php if(!is_default_lang()): ?> disabled <?php endif; ?> min="0" name="extra_price[<?php echo e($key); ?>][price]" class="form-control" value="<?php echo e($extra_price['price'] ?? ''); ?>">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <?php if(is_default_lang()): ?>
                                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="la la-trash"></i></span>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="text-right">
                                        <?php if(is_default_lang()): ?>
                                            <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> <?php echo e(__('Add item')); ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="g-more hide">
                                        <div class="item" data-number="__number__">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <?php if(!empty($languages) && setting_item('site_enable_multi_lang') && setting_item('site_locale')): ?>
                                                        <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php $key = setting_item('site_locale') != $language->locale ? "_".$language->locale : ""   ?>
                                                            <div class="g-lang">
                                                                <div class="title-lang"><?php echo e($language->name); ?></div>
                                                                <input type="text" __name__="extra_price[__number__][name<?php echo e($key); ?>]" class="form-control" value="" placeholder="<?php echo e(__('Extra price name')); ?>">
                                                            </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php else: ?>
                                                        <input type="text" __name__="extra_price[__number__][name]" class="form-control" value="" placeholder="<?php echo e(__('Extra price name')); ?>">
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="number" min="0" __name__="extra_price[__number__][price]" class="form-control" value="">
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="la la-trash"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ls-widget">
                    <div class="widget-title"><h4><?php echo e(__("Description")); ?></h4></div>
                    <div class="widget-content">
                        <div class="form-group">
                            <label><?php echo e(__("Briefly Describe Your Gig")); ?></label>
                            <textarea name="content" class="d-none has-ckeditor" cols="30" rows="10"><?php echo e($translation->content); ?></textarea>
                        </div>
                        <div class="form-group">
                            <label><?php echo e(__("Frequently Asked Questions")); ?></label>
                            <div class="form-group-item">
                                <div class="g-items-header">
                                    <div class="row">
                                        <div class="col-md-11"><?php echo e(__("Add Questions & Answers for Your Buyers.")); ?></div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                                <div class="g-items">
                                    <?php $old = old('faqs',$row->faqs ?? []);
                                    if(empty($old)) $old = [[]];
                                    ?>
                                    <?php if(!empty($old)): ?>
                                        <?php $__currentLoopData = $old; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$faq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="item" data-number="<?php echo e($key); ?>">
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <input type="text" name="faqs[<?php echo e($key); ?>][title]" class="form-control" value="<?php echo e($faq['title'] ?? ''); ?>" placeholder="<?php echo e(__('Add a Question: i.e. Do you translate to English as well?')); ?>">
                                                        <textarea name="faqs[<?php echo e($key); ?>][content]" class="form-control" placeholder="<?php echo e(__('Add an Answer: i.e. Yes, I also translate from English to Hebrew.')); ?>"><?php echo e($faq['content'] ?? ''); ?></textarea>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <span class="btn btn-danger btn-sm btn-remove-item"><i class="la la-trash"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                                <div class="text-right">
                                    <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> <?php echo e(__('Add FAQ')); ?></span>
                                </div>
                                <div class="g-more hide">
                                    <div class="item" data-number="__number__">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <input type="text" __name__="faqs[__number__][title]" class="form-control" placeholder="<?php echo e(__('Add a Question: i.e. Do you translate to English as well?')); ?>">
                                                <textarea __name__="faqs[__number__][content]" class="form-control" placeholder="<?php echo e(__('Add an Answer: i.e. Yes, I also translate from English to Hebrew.')); ?>"></textarea>
                                            </div>
                                            <div class="col-md-1">
                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="la la-trash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ls-widget">
                    <div class="widget-title"><h4><?php echo e(__("Requirements")); ?></h4></div>
                    <div class="widget-content">
                        <div class="form-group-item mb-4">
                            <div class="g-items-header">
                                <div class="row">
                                    <div class="col-md-11"><?php echo e(__("Add Question")); ?></div>
                                    <div class="col-md-1"></div>
                                </div>
                            </div>
                            <div class="g-items">
                                <?php $old = old('requirements',$row->requirements ?? []);
                                if(empty($old)) $old = [];
                                ?>
                                <?php if(!empty($old)): ?>
                                    <?php $__currentLoopData = $old; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$rq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="item" data-number="<?php echo e($key); ?>">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <textarea name="requirements[<?php echo e($key); ?>][content]" class="form-control" placeholder="<?php echo e(__('Request necessary details such as dimensions, brand guidelines, and more.')); ?>"><?php echo e($rq['content'] ?? ''); ?></textarea>
                                                </div>
                                                <div class="col-md-2">
                                                    <label ><input type="checkbox" <?php if($rq['required'] ?? ''): ?> checked <?php endif; ?> name="requirements[<?php echo e($key); ?>][required]"  value="1" > <?php echo e(__("Required")); ?></label>
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="la la-trash"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                            <div class="text-right">
                                <span class="btn btn-info btn-sm btn-add-item"><i class="icon ion-ios-add-circle-outline"></i> <?php echo e(__('Add question')); ?></span>
                            </div>
                            <div class="g-more hide">
                                <div class="item" data-number="__number__">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <textarea __name__="requirements[__number__][content]" class="form-control" placeholder="<?php echo e(__('Request necessary details such as dimensions, brand guidelines, and more.')); ?>"></textarea>
                                        </div>
                                        <div class="col-md-2">
                                            <label ><input type="checkbox" __name__="requirements[__number__][required]"  value="1" > <?php echo e(__("Required")); ?></label>
                                        </div>
                                        <div class="col-md-1">
                                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="la la-trash"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ls-widget">
                    <div class="widget-title"><h4><?php echo e(__("Gallery")); ?></h4></div>
                    <div class="widget-content">
                        <div class="form-group">
                            <label><?php echo e(__("Gallery")); ?></label>
                            <?php echo \Modules\Media\Helpers\FileHelper::fieldGalleryUpload('gallery',$row->gallery); ?>

                        </div>
                        <div class="form-group">
                            <label><?php echo e(__("Youtube Video")); ?></label>
                            <input type="text" name="video_url" class="form-control" value="<?php echo e(old('video_url',$row->video_url)); ?>" placeholder="<?php echo e(__("Youtube link video")); ?>">
                        </div>
                    </div>
                </div>

                <?php echo $__env->make('Core::frontend/seo-meta/seo-meta', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                <div class="mb-4 d-none d-md-block">
                    <button class="theme-btn btn-style-one" type="submit"><i class="fa fa-save" style="padding-right: 5px"></i> <?php echo e(__('Save Changes')); ?></button>
                </div>

            </div>

            <div class="col-xl-3">
                <div class="ls-widget">
                    <div class="widget-title"><h4><?php echo e(__("Publish")); ?></h4></div>
                    <div class="widget-content">
                        <div class="form-group">
                            <?php if(is_default_lang()): ?>
                                <div>
                                    <label><input <?php if($row->status=='publish'): ?> checked <?php endif; ?> type="radio" name="status" value="publish"> <?php echo e(__("Publish")); ?>

                                    </label></div>
                                <div>
                                    <label><input <?php if($row->status=='draft'): ?> checked <?php endif; ?> type="radio" name="status" value="draft"> <?php echo e(__("Draft")); ?>

                                    </label></div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <div class="text-right">
                                <button class="theme-btn btn-style-one" type="submit"><i class="fa fa-save"></i> <?php echo e(__('Save Changes')); ?></button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ls-widget">
                    <div class="widget-title"><h4><?php echo e(__("Feature Image")); ?></h4></div>
                    <div class="widget-content">
                        <div class="form-group">
                            <?php echo \Modules\Media\Helpers\FileHelper::fieldUpload('image_id',$row->image_id); ?>

                        </div>
                    </div>
                </div>

                <?php $__currentLoopData = $attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="ls-widget">
                        <div class="widget-title"><strong><?php echo e(__('Attribute: :name',['name'=>$attribute->name])); ?></strong></div>
                        <div class="widget-content">
                            <div class="form-group terms-scrollable">
                                <?php $__currentLoopData = $attribute->terms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <label class="term-item">
                                        <input <?php if(!empty($selected_terms) and $selected_terms->contains($term->id)): ?> checked <?php endif; ?> type="checkbox" name="terms[]" value="<?php echo e($term->id); ?>">
                                        <span class="term-name"><?php echo e($term->name); ?></span>
                                    </label>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script>
        jQuery(function ($) {
            "use strict"
            var on_load = true;
            $('[name=cat_id]').on('change',function (){
                $('[name="cat2_id"] option').show().hide();
                $('[name="cat2_id"] [data-parent="'+$(this).val()+'"]').show();
                if(!on_load){
                    $('[name="cat2_id"] option:eq(0)').prop('selected', true);
                    $('[name="cat3_id"] option:eq(0)').prop('selected', true);
                }
                $('[name="cat2_id"]').trigger("change");
                on_load = false;
            }).trigger('change')
            $('[name=cat2_id]').on('change',function (){
                $('[name="cat3_id"] option').show().hide();
                $('[name="cat3_id"] [data-parent="'+$(this).val()+'"]').show();
            }).trigger('change');

            $('.open-edit-input').on('click', function (e) {
                e.preventDefault();
                $(this).replaceWith('<input type="text" name="' + $(this).data('name') + '" value="' + $(this).html() + '">');
            });

            // Tag input
            $('.tag-input').on('keypress',function (e) {
                if(e.keyCode == 13){
                    var val = $(this).val();
                    if(val){
                        var html = '<span class="tag_item">' + val +
                            '       <span data-role="remove"></span>\n' +
                            '          <input type="hidden" name="tag_name[]" value="'+val+'">\n' +
                            '       </span>';
                        $(this).parent().find('.show_tags').append(html);
                        $(this).val('');
                    }
                    e.preventDefault();
                    return false;
                }
            });

            $(document).on('click','[data-role=remove]',function () {
                $(this).closest('.tag_item').remove();
            });

            $(".form-group-item").each(function () {
                let container = $(this);
                $(this).on('click','.btn-remove-item',function () {
                    $(this).closest(".item").remove();
                });

                $(this).on('press','input,select',function () {
                    let value = $(this).val();
                    $(this).attr("value",value);
                });
            });
            $(".form-group-item .btn-add-item").on('click',function () {
                var p = $(this).closest(".form-group-item").find(".g-items");

                let number = $(this).closest(".form-group-item").find(".g-items .item:last-child").data("number");
                if(number === undefined) number = 0;
                else number++;
                let extra_html = $(this).closest(".form-group-item").find(".g-more").html();
                extra_html = extra_html.replace(/__name__=/gi, "name=");
                extra_html = extra_html.replace(/__number__/gi, number);
                p.append(extra_html);

                if(extra_html.indexOf('dungdt-select2-field-lazy') >0 ){

                    p.find('.dungdt-select2-field-lazy').each(function () {
                        var configs = $(this).data('options');
                        $(this).select2(configs);
                    });
                }
            });

        })
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/serv5info/public_html/Job/modules/Gig/Views/frontend/seller/manageGig/edit.blade.php ENDPATH**/ ?>