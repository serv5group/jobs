<?php
$menu_active = $menu_active ?? '';
$menus = \Modules\User\ModuleProvider::getUserFrontendMenu();
if (!empty($menus)){
    foreach ($menus as $k => $menuItem) {
        if ((!empty($menuItem['permission']) and !\Auth::user()->hasPermission($menuItem['permission'])) or !$menuItem['enable'] ) {
            unset($menus[$k]);
            continue;
        }
        $menus[$k]['class'] = $menu_active == $k ? 'active' : '';
    }
}

?>
<div class="user-sidebar">
    <div class="sidebar-inner">
        <ul class="navigation">
            <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="<?php echo e($val['class']); ?>">
                    <a href="<?php echo e(home_url().'/'.$val['url']); ?>">
                        <i class="<?php echo e($val['icon']); ?>"></i> <?php echo e($val['title']); ?>

                    </a>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
</div>
<?php /**PATH /home2/serv5info/public_html/Job/modules/Layout/parts/sidebar.blade.php ENDPATH**/ ?>